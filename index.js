// Set up the dependencies
const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to be available to our front end application
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

// Server Setup
const app = express();
const port = 4000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.qift0pp.mongodb.net/B241-API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the Atlas"));


app.use("/users", userRoute);
app.use("/courses", courseRoute);


app.listen(port, () => console.log(`Now listening to port ${port}`));

