const express = require("express");
const router = express.Router();
const auth = require("../auth");
const courseController = require("../controllers/courseController");

// Route for creating a course
router.post("/addCourse", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
		if(userData.isAdmin){
		courseController.addCourse(req.body).then(resultFromController => {
			res.send(true);
		});
	} else {
		res.send("User must be ADMIN to access this.");
	}
});

// get all course
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => {
			res.send(resultFromController);
		}
	);
});

// get all active course
router.get("/allActive", (req, res) => {
	courseController.getAllActiveCourses().then(resultFromController => {
			res.send(resultFromController);
		}
	);
});

// get specific course
router.get("/:id", (req, res) => {
	courseController.getCourse(req.params.id).then(resultFromController => {
			res.send(resultFromController);
		}
	);
});

// Update a course
router.put("/:id", auth.verify, (req, res) => {

	courseController.updateCourse(req.params.id, req.body).then(resultFromController => {
			res.send(resultFromController);
		}
	);
});

// Archive a course
router.patch("/:id", auth.verify, (req, res) => {

	courseController.archiveCourse(req.params.id, req.body).then(resultFromController => {
			res.send(resultFromController);
		}
	);
});

module.exports = router;
