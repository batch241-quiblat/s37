const Course = require("../models/Course");

module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false;
		} else {
			return course;
		}
	});

	/*
	let message = Promise.resolve("User must be ADMIN to access this.");
	return message.then((value) => {
		return value;
	});*/
}

// get all course
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
} 

// get all active course
module.exports.getAllActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
} 

// get specific course
module.exports.getCourse = (courseID) => {
	return Course.findById(courseID).then(course => {
		return course;
	});
} 

// update a course
module.exports.updateCourse = (courseID, reqBody) => {

	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	/*
		SYNTAX:
		findByIdAndUpdate(document ID, UpdatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(courseID, updateCourse).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	});
} 

// archive a course
module.exports.archiveCourse = (courseID, reqBody) => {

	let updatestatus = {
		isActive: reqBody.isActive
	}

	return Course.findByIdAndUpdate(courseID, updatestatus).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	});
} 